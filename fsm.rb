#!/bin/ruby

class Fsm
  # define the transition states, pretty straightforward. each state has a transition for possible inputs, as well a "final" state
  STATES = {
    S0: { 0 => :S0, 1 => :S1, :final => 0},
    S1: { 0 => :S2, 1 => :S0, :final => 1},
    S2: { 0 => :S1, 1 => :S2, :final => 2},
  }

  # eg: Fsm.new("10101"). We're assuming numbers only. Not much error checking below.
  def initialize(input_string)
    @input_string = input_string
    @state = :S0
    @current_atom_index = 0
  end


  # state getter, so that we can inspect the FSM
  def state
    @state
  end

  # transition from state to state 
  def next_state

    # this is to handle a case where we end up with no input in a non-final state. which can't happen with our state machine but here for completeness.
    if end_of_input? && get_final_output == nil
      raise "Reached end of input but it's not a final state!"
    elsif end_of_input?
      return get_final_output
    end
    
    # get the next value in the input string
    input_value = @input_string.slice(@current_atom_index).to_i

    # figure out the next state
    @state = STATES[state][input_value]
    @current_atom_index = @current_atom_index + 1
    nil
  end

  # executes the fsm fully and returns the result
  def run
    begin
      result = next_state
    end while result == nil
    result
  end
  


  private

  # returns the final output
  def get_final_output
    STATES[state][:final]
  end


  # are we at the end of input?
  def end_of_input?
    @current_atom_index == @input_string.length
  end

  
end 
