#!/usr/bin/ruby
require "minitest/autorun"
require "./fsm.rb"


class TestFsm < Minitest::Test
  def setup
    @fsm = Fsm.new("110")
  end

  def test_initial_state
    assert_equal @fsm.state, :S0, "initial state should be S0"
  end

  def test_first_state
    result = @fsm.next_state
    assert_nil result, "Not expecting a final state at this point!"
    assert_equal :S1, @fsm.state, "expecting the second state to be S1"
  end

  def test_full_run
    result = @fsm.run
    assert_equal :S0, @fsm.state, "Expecting final state to be S0"
    assert_equal 0, result, "Expecting result to be 0!"
  end
end



class Test2Fsm < Minitest::Test
  def setup
    @fsm = Fsm.new("1010")
  end

  def test_initial_state
    assert_equal @fsm.state, :S0, "initial state should be S0"
  end


  def test_full_run
    result = @fsm.run
    assert_equal :S1, @fsm.state, "Expecting final state to be S1"
    assert_equal 1, result, "Expecting result to be 1!"
  end
end


