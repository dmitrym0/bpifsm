
## Problem

Write a program that implements the following FSA:

```
FSM will have 3 states
S0: Starting and final state (output value of 0)
S1: Final state (output value of 1)
S2: Final state (output value of 2)

State transition table is as follows:
Current state | Input | Result state
S0            | 0     | S0
S0            | 1     | S1
S1            | 0     | S2
S1            | 1     | S0
S2            | 0     | S1
S2            | 1     | S2

Example 1
for input string 110 the machine will go as follows.
Initial state = S0, Input = 1, result state = S1
Current state = S1, Input = 1, result state = S0
Current state = S0, Input = 0, result state = S0
- No more input
- Is current state final state? Yes then print output value (output for state S0 = 0)<---- This is the answer
- Is current state final state? No then print invalid input Note: this will never happen with our above machine as all state can be final state

Example 2
for input string 1010
Initial state = S0, Input = 1, result state = S1
Current state = S1, Input = 0, result state = S2
Current state = S2, Input = 1, result state = S2
Current state = S2, Input = 0, result state = S1
- No more input
- Is current state final state? Yes then print output value (output for state S1 = 1) <---- This is the answer
- Is current state final state? No then print invalid input Note: this will never happen with our above machine as all state can be final state
```


## Discussion

I was initially worried about implementation complexity since I've never worked with FSM before.

My first instinct was to look for an existing library in [ruby toolbox](https://www.ruby-toolbox.com/categories/state_machines). I identified [state-machine](https://github.com/state-machines/state_machines) gem as a potential library to use. While reading the docs it occured to me that state-machine may be an overkill for the problem at hand. It did serve as a review on how FSM function though, so it wasn't effort lost.

I decided to use the minimum amount of code I could get away with, in Ruby.

I defined state transition in a class constant (`STATES`). I cheated a bit by introducing a `final` key. This is the value of the "result" for each state.

I then used TDD to design the rest of the code. `Fsm` class in `fsm.rb` implements the functionality, and `TestFsm` and `Test2Fsm` in `test.rb` are used to test the code.

Tests in [test.rb](./test.rb) pretty much tell the story of the design.

In TestFsm:

1. In `test_initial_sate`, I test that the initial state is set correctly when object is initialized
2. In `test_first_state`, I test that that the first transition happens as described in the problem statement AND that no result is returned (since it's not the final state)
3. In `test_full_run`, I execute the FSM end to end and test the final state and the remaind (6 % 3 = 0)

In Test2Fsm:

1. In `test_initial_state`, I verify that the initial state is correct 
2. In `test_full_run`, I execute exercise the FSM fully (10 % 3 = 1)


I make a couple of class methods public (`next_state`, `state`) to facilitate testing. I could've made them private, but it's not clear how this class is to be used so this does expose some plumbing which is not something I like.


## Installation

This code requires:

- Ruby 2.3.3
- Minitest 5.11

It can be manually installed with `gem install minitest`.

Alternatively, install Bundler:

`gem install bundler`

then bundler will install all dependencies:

`bundle install`



## Execution

If you used bundler:


`bundle exec ruby test.rb` to execute test cases.

alternatively


`ruby test.rb` should suffice:


```
~/w/bpi ❯❯❯ ruby test.rb                                                                                                                                                                                                    
Run options: --seed 10100

# Running:

.....

Finished in 0.001535s, 3257.3290 runs/s, 5211.7264 assertions/s.

5 runs, 8 assertions, 0 failures, 0 errors, 0 skips
````